# Docker Monitoring Stack

This stack is for use on a **Raspberry PI ARM64**.

Tools usable in this stack:

- cAdvisor _(specific for RaspberryPI 4 ARM64)_
- node-exporter
- Prometheus
- Telegraf
- InfluxDB 2
- Grafana

**Setup explanations are inside compose file**.

Don't forget to follow setup instructions, copy all files in correct folders and set permissions.

You also need to edit all files in 'config' folder to adjust settings like

- username
- password
- organization
- bucket
- admin token
- ...

All you need to edit is in _[bla bla]_ form.

**This should give you** something like this:

![alt text](docs/sample.png "Sample run")
